<?php
/**
 * Template Name: Ebooks
 *
 * Page template to display the advanced page builder and video content.
 *
 * @package boxpress
 */
get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="section">
    <div class="wrap">
      <div class="l-sidebar">
        <div class="l-main-col">
          <div class="ebook-section">
             <?php

             if( have_rows('ebook_row') ): ?>

             <div class="l-grid l-grid--three-col">

              <?php   while ( have_rows('ebook_row') ) : the_row();  ?>

                  <?php get_template_part( 'template-parts/content/content-ebooks' ); ?>

              <?php endwhile; ?>
            </div>
            <?php endif; ?>
          </div>
        </div>
        <div class="l-aside-col">
          <div class="sidebar-widget">
            <nav class="sidebar-nav">
              <ul>
                <?php
                wp_list_pages(array(
                  'child_of' => '93',
                  'depth' => 1,
                  'title_li' => "",
                ));
                ?>
              </ul>
            </nav>
          </div>
        </div>
      </div>


    </div>
  </section>

<?php get_footer(); ?>
