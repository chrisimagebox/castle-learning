<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package boxpress
 */
?>
</main>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="section primary-footer">
      <div class="wrap">
        <div class="footer-box">
          <div class="site-branding">
            <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
              <span class="vh"><?php bloginfo('name'); ?></span>
              <svg class="site-logo" width="200" height="38" focusable="false">
                <use href="#site-logo"/>
              </svg>
            </a>
          </div>
        </div>
        <!-- col 1 -->
        <div class="footer-box middle">
        <div class="l-grid--four-col">
            <div class="l-grid-item ">
              <h6>Quick Links</h6>
            <?php // Utility Navigation ?>
            <?php if ( has_nav_menu( 'quicklinks' )) : ?>
              <nav class="navigation--utility"
                aria-label="<?php _e( 'Utility Navigation', 'boxpress' ); ?>"
                role="navigation">
                <ul class="nav-list">
                  <?php
                    wp_nav_menu( array(
                      'theme_location'  => 'quicklinks',
                      'items_wrap'      => '%3$s',
                      'container'       => false,
                      'walker'          => new Aria_Walker_Nav_Menu(),
                    ));
                  ?>
                </ul>
              </nav>
            <?php endif; ?>
          </div>
          <div class="l-grid-item">
            <h6>Legal Links</h6>
            <?php // Footer Navigation ?>
            <?php if ( has_nav_menu( 'legallinks' )) : ?>
                <nav class="navigation--utility"
                  aria-label="<?php _e( 'Footer Navigation', 'boxpress' ); ?>"
                  role="navigation">
                  <ul class="nav-list">
                    <?php
                      wp_nav_menu( array(
                        'theme_location'  => 'legallinks',
                        'items_wrap'      => '%3$s',
                        'container'       => false,
                        'walker'          => new Aria_Walker_Nav_Menu(),
                      ));
                    ?>
                  </ul>
                </nav>
            <?php endif; ?>
          </div>
          <div class="l-grid-item">
            <h6>Contact</h6>
              <?php get_template_part( 'template-parts/global/address-block' ); ?>
          </div>
          <div class="l-grid-item">
            <h6>Social Media</h6>
              <?php get_template_part( 'template-parts/global/social-nav' ); ?>
          </div>
        </div>
       </div>
       <!-- col 2 -->
        <div class="footer-box">
          <?php // Footer Navigation ?>
          <?php if ( has_nav_menu( 'footerbuttons' )) : ?>
            <div class="l-footer-item ">
              <nav class="navigation--utility footerbuttons"
                aria-label="<?php _e( 'Footer Navigation', 'boxpress' ); ?>"
                role="navigation">
                <ul class="nav-list">
                  <?php
                    wp_nav_menu( array(
                      'theme_location'  => 'footerbuttons',
                      'items_wrap'      => '%3$s',
                      'container'       => false,
                      'walker'          => new Aria_Walker_Nav_Menu(),
                    ));
                  ?>
                </ul>
              </nav>
            </div>
          <?php endif; ?>
        </div>
        <!-- col 3 -->
  </div>
</div>

    <div class="site-info">
      <div class="wrap">
      <div class="l-footer l-footer--2-cols l-footer--gap-large">
        <div class="l-footer-item">
          <div class="site-branding">
            <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
              <span class="vh"><?php bloginfo('name'); ?></span>
              <svg class="hes-logo" width="120" height="38" focusable="false">
                <use href="#hes-logo"/>
              </svg>
            </a>
          </div>
          <div class="site-copyright">
            <p>
              <small>
                <?php _e('Copyright', 'boxpress'); ?> &copy; <?php echo date('Y'); ?>
                <?php
                  $company_name     = get_bloginfo( 'name', 'display' );
                  $alt_company_name = get_field( 'alternative_company_name', 'option' );

                  if ( ! empty( $alt_company_name )) {
                    $company_name = $alt_company_name;
                  }
                ?>
                <?php echo $company_name; ?>.
                <?php _e('All rights reserved.', 'boxpress'); ?>
              </small>
            </p>
          </div>
        </div>
        <div class="l-footer-item">
          <div class="imagebox">
            <p>
              <small>
                <?php _e('Website by', 'boxpress'); ?>
                <a href="https://imagebox.com" target="_blank">
                  <span class="vh">Imagebox</span>
                  <svg class="imagebox-logo-svg" width="81" height="23" focusable="false">
                    <use href="#imagebox-logo"/>
                  </svg>
                </a>
              </small>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>


<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>
