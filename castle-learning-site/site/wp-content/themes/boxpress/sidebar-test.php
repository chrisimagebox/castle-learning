<?php
/**
 * Displays the sidebar
 *
 * @package boxpress
 */
?>
<aside class="sidebar" role="complementary">

  <?php if ( is_home() ) :
      /**
       * Query for Child Pages
       * ---
       * If we've already queries for the child
       * pages, don't query again.
       */
      if ( ! isset( $child_pages_list )) {
        $child_pages_list = query_for_child_page_list();
      }
    ?>
    <?php if ( $child_pages_list ) : ?>

      <div class="sidebar-widget">
        <nav class="sidebar-nav">
          <ul>

            <?php echo $child_pages_list; ?>

          </ul>
        </nav>
      </div>

    <?php endif; ?>
  <?php endif; ?>


  <?php // Blog Archive + Post ?>




</aside>
