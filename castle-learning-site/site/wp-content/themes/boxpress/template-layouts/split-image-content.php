<?php
/**
 * Displays the slipt content / image layout
 *
 * @package boxpress
 */

$split_content    = get_sub_field( 'split_content' );
$split_image      = get_sub_field( 'split_image' );
$split_bkg        = get_sub_field( 'split_background' );
$split_image_position = get_sub_field( 'split_image_position' );
$split_image_size = 'block_half_width';

?>
<section class="split-block-layout <?php echo $split_bkg; ?> split-block-layout--<?php echo $split_image_position; ?>">
  <div class="split-block-col">

    <?php if ( $split_image ) : ?>

      <img class="split-block-image"
        src="<?php echo esc_url( $split_image['sizes'][ $split_image_size ] ); ?>"
        width="<?php echo esc_attr( $split_image['sizes'][ $split_image_size . '-width'] ); ?>"
        height="<?php echo esc_attr( $split_image['sizes'][ $split_image_size . '-height'] ); ?>"
        alt="<?php echo esc_attr( $split_image['alt'] ); ?>">

    <?php endif; ?>

  </div>
  <div class="split-block-col">
    <div class="split-block-content">
      <div class="split-block-content-inner-wrap">
        <?php echo $split_content; ?>
      </div>
    </div>
  </div>
</section>
