<?php
/**
 * Displays the full width column layout
 *
 * @package boxpress
 */



// Load sidebar if this is the first template & child pages exist
$is_first_row = ( $row_index == 1 ) ? true : false;

if ( $is_first_row ) {
  $child_pages_list = query_for_child_page_list();
} else {
  // Empty the child list array to prevent false positive
  $child_pages_list = array();
}

?>


  <section class="fullwidth-layout advanced-full-width section">
    <div class="wrap <?php
        if ( ! $child_pages_list ) {
          echo ' wrap--limited ';
        }
      ?>">
      <div class="<?php
          if ( $child_pages_list ) {
            echo 'l-sidebar';
          }
        ?>">
        <div class="l-main-col">
          <?php if ( $is_first_row ) : ?>
            <?php
               $media_headline = get_field( 'media_headline', get_the_ID() );
               $page_title = ( ! empty( $media_headline )) ? $media_headline : get_the_title( get_the_ID() );
             ?>
             <header class="page-header">
               <h1 class="page-title"><?php echo $page_title; ?></h1>
             </header>

          <?php endif; ?>

          <div class="accordion-block">
            <h2 id="elementary-block"><?php echo $curriculum_elementary_title; ?></h2>
           <?php if ( have_rows ('accordion_row') ): ?>
             <?php while ( have_rows ('accordion_row') ) : the_row(); ?>
               <?php $accord_title = get_sub_field('accord_title'); ?>
               <?php $accord_content = get_sub_field('accord_content'); ?>

               <button class="accordion accord-is-open"><?php echo $accord_title; ?>
                 <span></span>
               </button>

               <div class="accordion-content">
                 <p><?php echo $accord_content; ?></p>
               </div>
            <?php endwhile; ?>
          </div>
         <?php endif; ?>


        </div>

        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>

      </div>
    </div>

    <?php if ( $content_bkg_image && $content_bkg === 'background-image' ) : ?>
      <img class="fullwidth-layout-bkg" draggable="false" aria-hidden="true"
        src="<?php echo esc_url( $content_bkg_image['sizes'][ $content_bkg_size ] ); ?>"
        width="<?php echo esc_attr( $content_bkg_image['sizes'][ $content_bkg_size . '-width'] ); ?>"
        height="<?php echo esc_attr( $content_bkg_image['sizes'][ $content_bkg_size . '-height'] ); ?>"
        alt="">
    <?php endif; ?>

  </section>
