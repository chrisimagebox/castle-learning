<?php
/**
 * Displays the full width image block layout
 *
 * @package boxpress
 */

$photo_image  = get_sub_field( 'photo_block_image' );
$is_parallax  = get_sub_field( 'enable_parallax' );
$photo_image_size = 'block_full_width';

?>
<?php if ( $photo_image ) : ?>

  <section class="photo-block-layout <?php
      if ( $is_parallax ) {
        echo 'photo-block-layout--parallax';
      }
    ?>" style="background-image:url('<?php echo esc_url( $photo_image['sizes'][ $photo_image_size ] ); ?>');">
  </section>

<?php endif; ?>
