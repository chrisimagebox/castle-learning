<?php
/**
 * Displays the button block layout
 *
 * @package boxpress
 */

$button_block_title = get_sub_field( 'button_block_title' );
$button_block_copy  = get_sub_field( 'button_block_copy' );
$button_block_link  = get_sub_field( 'button_block_link' );
$button_block_bkg   = get_sub_field( 'button_block_background' );
$button_block_bkg_image = get_sub_field( 'button_block_background_image' );
$button_block_bkg_size  = 'block_full_width';

?>
<section class="button-block-layout section <?php echo $button_block_bkg; ?>">
  <div class="wrap wrap--limited">
    <div class="button-block">

      <?php if ( ! empty( $button_block_title )) : ?>

        <div class="callout-header">
          <h2><?php echo $button_block_title; ?></h2>
        </div>

      <?php endif; ?>

      <div class="callout-body">
        <div class="button-callout-content">
          <?php if ( ! empty( $button_block_copy )) : ?>
            <p><?php echo $button_block_copy; ?></p>
          <?php endif; ?>

          <?php if ( $button_block_link ) : ?>
            <a class="button"
              href="<?php echo esc_url( $button_block_link['url'] ); ?>"
              target="<?php echo esc_attr( $button_block_link['target'] ); ?>">
              <?php echo $button_block_link['title']; ?>
            </a>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
  <?php if ( $button_block_bkg === 'background-image' && $button_block_bkg_image ) : ?>
    <img class="button-block-layout-bkg" draggable="false" aria-hidden="true"
      src="<?php echo esc_url( $button_block_bkg_image['sizes'][ $button_block_bkg_size ] ); ?>"
      width="<?php echo esc_attr( $button_block_bkg_image['sizes'][ $button_block_bkg_size . '-width' ] ); ?>"
      height="<?php echo esc_attr( $button_block_bkg_image['sizes'][ $button_block_bkg_size . '-height' ] ); ?>"
      alt="">
  <?php endif; ?>
</section>
