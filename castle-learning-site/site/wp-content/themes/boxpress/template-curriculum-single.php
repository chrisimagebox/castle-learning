<?php
/**
 * Template Name: Curriculum Single
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php // Hero ?>

    <?php require_once('template-parts/banners/banner--curriculum.php'); ?>


    <?php
    $curriculum_single_intro    = get_field( 'curriculum_single_intro' );
    $curriculum_elementary_title   = get_field( 'curriculum_elementary_title' );
    $curriculum_middleschool_title   = get_field( 'curriculum_middleschool_title' );
    $curriculum_highschool_title   = get_field( 'curriculum_highschool_title' );
    ?>

    <section class="section curic_intro">
      <div class="wrap wrap--limited">
        <?php  echo $curriculum_single_intro; ?>
      </div>
    </section>


<section class="section curic-section color-option-5">
  <div class="wrap">
    <div class="curic-container">
      <div class="accordion-block">
        <h2 id="elementary-block"><?php echo $curriculum_elementary_title; ?></h2>
       <?php if ( have_rows ('accordion_row_elementary') ): ?>
         <?php while ( have_rows ('accordion_row_elementary') ) : the_row(); ?>
           <?php $accord_title = get_sub_field('accord_title'); ?>
           <?php $accord_content = get_sub_field('accord_content'); ?>

           <button class="accordion accord-is-open"><?php echo $accord_title; ?>
             <span></span>
           </button>

           <div class="accordion-content">
             <p><?php echo $accord_content; ?></p>
           </div>
        <?php endwhile; ?>
      </div>
     <?php endif; ?>

       <div class="pdf-block">
        <?php

        $pdf_text_elementary = get_field('pdf_text_elementary');
         ?>

         <?php echo $pdf_text_elementary; ?>
       </div>
    </div>
  </div>
</section>

<section class="section curic-section">
  <div class="wrap">
    <div class="curic-container">
      <div class="accordion-block">
        <h2 id="middleschool-block"><?php echo $curriculum_middleschool_title; ?></h2>
       <?php if ( have_rows ('accordion_row_middleschool') ): ?>
         <?php while ( have_rows ('accordion_row_middleschool') ) : the_row(); ?>
           <?php $accord_title = get_sub_field('accord_title'); ?>
           <?php $accord_content = get_sub_field('accord_content'); ?>

           <button class="accordion accord-is-open"><?php echo $accord_title; ?>
             <span></span>
           </button>

           <div class="accordion-content">
             <p><?php echo $accord_content; ?></p>
           </div>
        <?php endwhile; ?>
      </div>
     <?php endif; ?>

       <div class="pdf-block">
        <?php

        $pdf_text_middleschool = get_field('pdf_text_middleschool');
         ?>
         <?php echo $pdf_text_middleschool; ?>
       </div>
    </div>
  </div>
</section>

<section class="section curic-section color-option-5">
  <div class="wrap">
    <div class="curic-container">
      <div class="accordion-block">
        <h2 id="highschool-block"><?php echo $curriculum_highschool_title; ?></h2>
       <?php if ( have_rows ('accordion_row_highschool') ): ?>
         <?php while ( have_rows ('accordion_row_highschool') ) : the_row(); ?>
           <?php $accord_title = get_sub_field('accord_title'); ?>
           <?php $accord_content = get_sub_field('accord_content'); ?>

           <button class="accordion accord-is-open"><?php echo $accord_title; ?>
             <span></span>
           </button>

           <div class="accordion-content">
             <p><?php echo $accord_content; ?></p>
           </div>
        <?php endwhile; ?>
      </div>
     <?php endif; ?>

       <div class="pdf-block">
        <?php

        $pdf_text_highschool = get_field('pdf_text_highschool');
         ?>


         <?php echo $pdf_text_highschool; ?>
       </div>
    </div>
  </div>
</section>

<?php

$button_block_title  = get_field( 'button_block_title' );
$button_block_link_curic  = get_field( 'button_block_link_curic' );

?>
<section class="button-block-layout color-option-4 section">
  <div class="wrap wrap--limited">
    <div class="button-block">
      <?php if ( ! empty( $button_block_title )) : ?>

        <div class="callout-header">
          <h2><?php echo $button_block_title; ?></h2>
        </div>

      <?php endif; ?>
      <div class="callout-body">
        <div class="button-callout-content">
          <?php if ( $button_block_link_curic ) : ?>
            <a class="button"
              href="<?php echo esc_url( $button_block_link_curic['url'] ); ?>"
              target="<?php echo esc_attr( $button_block_link_curic['target'] ); ?>">
              <?php echo $button_block_link_curic['title']; ?>
            </a>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
</section>


</article>

<?php get_footer(); ?>
