<?php
/**
 * Template Name: Curriculum
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php // Hero ?>

    <?php require_once('template-parts/banners/banner--curriculum.php'); ?>


    <?php
    $curriculum_content    = get_field( 'curriculum_content' );

    $curriculum_extra = get_field('curriculum_extra');
    $curriculum_extra_col = get_field('curriculum_extra_col');
    ?>


<section class="section curic_intro">
  <div class="wrap wrap--limited">
    <?php  echo $curriculum_content ?>
  </div>
</section>

<section class="section curic_section">
  <div class="wrap">
      <div class="l-grid l-grid--four-col">
        <?php if ( have_rows( 'curic_row' )) : ?>
          <?php while ( have_rows( 'curic_row' )) : the_row();
          $curriculum_icon = get_sub_field('curriculum_icon');
          $curriculum_link = get_sub_field('curriculum_link');
          $curriculum_link_anchor_1 = get_sub_field('curriculum_link_anchor_1');
          $curriculum_link_anchor_2 = get_sub_field('curriculum_link_anchor_2');
          $curriculum_link_anchor_3 = get_sub_field('curriculum_link_anchor_3');
          $curriculum_description = get_sub_field('curriculum_description');
           ?>
            <div class="curic_col l-grid-item">
              <?php if ( $curriculum_link ) : ?>
                <a class="link-box" href="<?php echo esc_url( $curriculum_link['url'] ); ?>"
                      target="<?php echo esc_attr( $curriculum_link['target'] ); ?>">
                  <img src="<?php echo $curriculum_icon; ?>" alt="<?php echo $curriculum_icon; ?>">
                  <h4><?php echo $curriculum_link['title']; ?></h4>
                  <p><?php echo $curriculum_description; ?></p>
                </a>
              <?php endif; ?>

              <div class="curic_col_links">
                <h6>Education Level</h6>
                <a href="<?php echo esc_url( $curriculum_link_anchor_1['url'] ); ?>"
                      target="<?php echo esc_attr( $curriculum_link_anchor_1['target'] ); ?>">
                  Elementary
                </a>
                <a href="<?php echo esc_url( $curriculum_link_anchor_2['url'] ); ?>"
                      target="<?php echo esc_attr( $curriculum_link_anchor_2['target'] ); ?>">
                  Middle School
                </a>
                <a href="<?php echo esc_url( $curriculum_link_anchor_3['url'] ); ?>"
                      target="<?php echo esc_attr( $curriculum_link_anchor_3['target'] ); ?>">
                  High School
                </a>
              </div>
            </div>
          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
</section>

<section class="section extra_section">
  <div class="wrap">
    <?php
    $extra_text = get_field('extra_text');
     ?>

    <div class="extra-text">
      <?php echo $extra_text; ?>
    </div>

     <div class="extra-box">
     <div class="l-grid l-grid--five-col">
       <?php if ( have_rows( 'extra_row' )) : ?>
         <?php while ( have_rows( 'extra_row' )) : the_row();
         $extra_icon = get_sub_field('extra_icon');
         $extra_text_row = get_sub_field('extra_text_row');
         ?>
            <div class="extra-icon l-grid-item">
            <a
              href="<?php echo esc_url( $extra_text_row['url'] ); ?>"
              target="<?php echo esc_attr( $extra_text_row['target'] ); ?>">
              <img src="<?php echo $extra_icon; ?>" alt="<?php echo $extra_icon; ?>">
              <?php echo $extra_text_row['title']; ?>
            </a>
            </div>
           <?php endwhile; ?>
         <?php endif; ?>
     </div>
    </div>
  </div>
</section>
1
</article>

<?php get_footer(); ?>
