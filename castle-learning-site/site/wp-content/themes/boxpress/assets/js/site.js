(function ($) {
  'use strict';

  /**
   * Site javascripts
   * ===
   */

  // Wrap video embeds in Flexible Container
  $('iframe[src*="youtube.com"]:not(.not-responsive), iframe[src*="vimeo.com"]:not(.not-responsive)')
    .attr( 'frameborder', 0 )
    .wrap('<div class="flexible-container"></div>');


    $('.video-button').magnificPopup({
      type: 'iframe',
      iframe: {
        patterns: {
          wistia: {
            index: 'wistia.com',
            id: function(url) {
                var m = url.match(/^.+wistia.com\/(medias)\/([^_]+)[^#]*(#medias=([^_&]+))?/);
                if (m !== null) {
                    if(m[4] !== undefined) {
                        return m[4];
                    }
                    return m[2];
                }
                return null;
            },
            src: '//fast.wistia.net/embed/iframe/%id%?autoPlay=1' // https://wistia.com/doc/embed-options#options_list
          }
        }
      }
    });

    $('.open-form').magnificPopup({type: 'inline'});
    // homepage new slide






  // Resize Maps
  $('iframe[src*="google.com/map"]:not(.not-responsive)')
    .attr( 'frameborder', 0 )
    .wrap('<div class="flexible-container"></div>');

    $('.video-sort ul li:first-child').addClass('current');
    $('.video-group .video-type:first-child').addClass('current');
    $('.video-sort li').click(function(){
		var video_tab_id = $(this).attr('data-tab-video-type');
        console.log(video_tab_id);
		$('.video-sort ul li').removeClass('current');
		$('.video-type').removeClass('current');
		$(this).addClass('current');
		$("#"+video_tab_id).addClass('current');
	});



})(jQuery);

var accordions = document.getElementsByClassName('accordion');

for (var i = 0; i < accordions.length; i++){
  accordions[i].onclick = function () {
    this.classList.toggle('accord-is-open');
    var content = this.nextElementSibling;

    if (content.style.maxHeight) {
        content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  };
}
