(function ($) {
  'use strict';

  /**
   * Site Carousels
   * ===
   */

  /**
   * Hero Carousel
   * ---
   * Tiny Slider
   * [https://github.com/ganlanyuan/tiny-slider#options]
   */

  var sliders = document.querySelectorAll( '.js-carousel' );
  var slider_init;

  if ( sliders.length ) {
    sliders.forEach(function (slider) {
      slider_init = tns({
        container: slider,
        mouseDrag: true,
        speed: 600,
        autoplay: false,
        controlsText: [
          '<span class="vh">Previous</span><svg class="svg-left-icon" width="40" height="40" focusable="false"><use href="#arrow-left-icon"/></svg>',
          '<span class="vh">Next</span><svg class="svg-right-icon" width="40" height="40" focusable="false"><use href="#arrow-right-icon"/></svg>',
        ]
      });
    });
  }

  var $q_testimonials = $('.q_testimonials');
    var q_testimonials_init;

  if ( $q_testimonials.length ) {
    q_testimonials_init = tns({
      container: '.q_testimonials',
      mouseDrag: true,
      speed: 800,
      autoplay: true,
      mode: 'gallery',
      autoHeight: true,
      responsive: {
       0 : {
         items: 1,
       },
       480 : {
         items: 1,
       },
       1200: {
         items: 1,
       }
      },
      controlsText: [
        '<span class="left-arrow-q"></span></svg>',
        '<span class="right-arrow-q"></span></svg>',
      ]
    });
  }



})(jQuery);
