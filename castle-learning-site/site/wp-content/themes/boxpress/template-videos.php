<?php
/**
 * Template Name: Videos
 *
 * Page template to display the advanced page builder and video content.
 *
 * @package boxpress
 */
get_header(); ?>

  <?php require_once('template-parts/banners/banner--page.php'); ?>


    <?php
      /**
       * Run clean shortcode for repeater sub-fields.
       *
       * Needs to be calld directly before repeaters, won't
       * work directly in functions.php because of how
       * sub-fields are called.
       */
      function boxpress_clean_shortcodes_acf( $content ) {

        // els to remove
        $array = array(
          '<p>['    => '[',
          ']</p>'   => ']',
          '<div>['  => '[',
          ']</div>' => ']',
          ']<br />' => ']',
          ']<br>'   => ']',
          '<br />[' => '[',
          '<br>['   => '[',
        );

        // Remove dem els
        $content = strtr( $content, $array );
        return $content;
      }
      add_filter('acf_the_content', 'boxpress_clean_shortcodes_acf');
    ?>

    <?php if ( have_rows( 'innerpage_master' )) :
        $row_index = 1;
      ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php while ( have_rows( 'innerpage_master' )) : the_row(); ?>

          <?php
            // Create layout string
            $layout_path = 'template-layouts/' . str_replace( '_', '-', get_row_layout()) . '.php';

            // Include our layout
            if (( @include $layout_path ) === false ) {
              // If the layout block is missing, display an error block only for admins
              if ( current_user_can('edit_others_pages') ) {
                echo '<div class="section boxpress-error"><div class="wrap wrap--limited"><p>Sorry bud, that layout file is missing!</p></div></div>';
              }
            }

            $row_index++;
          ?>

        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>

        <?php
          // Display sidebar at bottom of page for mobile
          $child_pages_list = query_for_child_page_list();
        ?>
        <?php if ( $child_pages_list ) : ?>

          <div class="advanced-sidebar">
            <div class="wrap">
              <?php include( get_template_directory() . '/sidebar.php'); ?>
            </div>
          </div>

        <?php endif; ?>

      </article>

    <?php endif; ?>

<!-- Add advance template -->

  <section class="video-tuts" id="video-section">
    <div class="video-sort">
      <?php
        $video_terms_nav = get_terms( 'video_type', array('order' => 'DESC') );
        $video_terms = get_terms( 'video_type', array('order' => 'DESC') );
      ?>
     <div class="wrap">
       <ul>
         <?php
           foreach ( $video_terms_nav as $video_term_nav ) { ?>
           <li data-tab-video-type="video-type-<?php echo $video_term_nav->slug; ?>"><?php echo $video_term_nav->name; ?></li>
         <?php } ?>
       </ul>
     </div>
    </div>

    <div class="wrap">
      <div class="section video-group">
      <?php
        foreach ( $video_terms as $video_term ) { ?>

        <div class="video-type video-type-<?php echo $video_term->slug; ?>" id="video-type-<?php echo $video_term->slug; ?>">
          <div class="wrap">
            <div class="l-grid l-grid--four-col">
          <?php
            $the_query = new WP_Query( array(
                'post_type' => 'video',
                'posts_per_page' => -1,
                'tax_query' => array(
                  array (
                    'taxonomy' => 'video_type',
                    'field' => 'slug',
                    'terms' => array( $video_term->slug ),
                    'operator' => 'IN'
                  )
                ),
            ) );
            while ( $the_query->have_posts() ) : $the_query->the_post();
              get_template_part( 'template-parts/content/content-video' );
            endwhile;
            echo '  </div>
              </div>
            </div>';
            $the_query = null;
            wp_reset_postdata();
          }
          ?>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
