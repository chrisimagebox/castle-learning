<?php
/**
 * The template for displaying archive pages.
 *
 * Template Name: Board Members
 *
 * @package boxpress
 */


get_header(); ?>


<section class="section about-grid">
  <div class="wrap">
    <header>
      <h1>About</h1>
    </header>
    <div class="l-sidebar">
      <div class="l-aside-col">
        <?php get_sidebar('about'); ?>
    </div>
    <div class="l-main-col">

      <?php
        $board_member_type_terms_args = array(
          'taxonomy'   => 'board_member_type',
          'hide_empty' => false,
        );
        $board_member_type_terms = get_terms( $board_member_type_terms_args );
      ?>

        <?php if ( $board_member_type_terms && ! is_wp_error( $board_member_type_terms )  ) : ?>
          <?php foreach ( $board_member_type_terms as $term ) : ?>
            <?php
             $term_slug = $term->slug;
             $term_name = $term->name;
            ?>


            <?php
              $board_query_args = array(
                'post_type' => 'board_members',
                'posts_per_page' => -1,
                'tax_query' => array(
                  array(
                    'taxonomy' => 'board_member_type',
                    'field'    => 'slug',
                    'terms'    => $term_slug,
                  ),
                ),
              );
              $board_query = new $wp_query( $board_query_args );
            ?>

          <?php if ( $board_query->have_posts() ) : ?>
            <section class="section staff-section">
              <div class="wrap">
                <header>
                  <h3><?php echo $term_name; ?></h3>
                </header>
                <div class="l-grid l-grid--three-col">
                  <?php while ( $board_query->have_posts() ) : $board_query->the_post(); ?>
                    <div class="l-grid-item">

                      <?php
                      $board_title = get_field('board_title');
                      ?>

                      <div class="card-shadow">
                      <?php if  ( has_post_thumbnail() ) : ?>
                        <div class="thumbnail">
                          <?php the_post_thumbnail('blog-thumb');?>
                        </div>
                      <?php endif; ?>
                        <div class="card-body">
                          <span><?php the_field('press_text'); ?></span>
                          <h3><?php the_title(); ?></h3>
                          <span class="board-title"><?php echo $board_title; ?></span>
                          <?php the_excerpt(); ?>
                          <div class="card-footer">
                            <a class="button button--green open-bio" href="#popup-<?php echo get_the_ID(); ?>">
                              Read More
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="popup-<?php echo get_the_ID(); ?>" class="mfp-hide popup">
                      <div class="wrap modal-wrap">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button title="Close (Esc)" type="button" class="mfp-close">  <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
                              <svg class="menu-icon-svg" width="20" height="20" focusable="false">
                                <use href="#close-icon"/>
                              </svg><span class="custom-close"></span></button>
                             <h2><?php the_title(); ?></h2>
                             <?php the_content(); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endwhile; ?>
                </div>
                <hr>
              </div>
            </section>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>

        <?php endforeach; ?>

      <?php endif; ?>
      <!-- end office  -->



    </div>
  </div>
</div>
</section>


<?php get_footer(); ?>
