<?php
/**
 * Team Custom Post Type
 *
 * @package boxpress
 */

function cpt_video() {

  $label_plural   = 'Video Tutorials';
  $label_singular = 'Video Tutorial';

  $labels = array(
    'name'                  => _x( "{$label_plural}", 'Post Type General Name', 'boxpress' ),
    'singular_name'         => _x( "{$label_singular}", 'Post Type Singular Name', 'boxpress' ),
    'menu_name'             => __( "{$label_plural}", 'boxpress' ),
    'name_admin_bar'        => __( "{$label_singular}", 'boxpress' ),
    'parent_item_colon'     => __( "Parent {$label_singular}:", 'boxpress' ),
    'all_items'             => __( "All {$label_plural}", 'boxpress' ),
    'add_new_item'          => __( "Add New {$label_singular}", 'boxpress' ),
    'add_new'               => __( "Add New", 'boxpress' ),
    'new_item'              => __( "New {$label_singular}", 'boxpress' ),
    'edit_item'             => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'           => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'             => __( "View {$label_singular}", 'boxpress' ),
    'search_items'          => __( "Search {$label_singular}", 'boxpress' ),
    'not_found'             => __( "Not found", 'boxpress' ),
    'not_found_in_trash'    => __( "Not found in Trash", 'boxpress' ),
    'items_list'            => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation' => __( "{$label_plural} list navigation", 'boxpress' ),
    'filter_items_list'     => __( "Filter {$label_plural} list", 'boxpress' ),
  );

  $rewrite = array(
		'slug'                  => 'video-tutorials',
    'with_front'            => false,
	);

  $args = array(
    'label'                 => __( "{$label_plural}", 'boxpress' ),
    'description'           => __( "{$label_singular} Custom Post Type", 'boxpress' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'          => false,
    'can_export'                 => true,
    'has_archive'                => false,
    'exclude_from_search'        => true,
    'publicly_queryable'         => true,
    'show_tagcloud'              => false,
    'rewrite'               => $rewrite,
    'menu_icon'             => 'dashicons-video-alt3',
    'capability_type'       => 'post',
  );

  register_post_type( 'video', $args );
}
add_action( 'init', 'cpt_video', 0 );


/**
 * Create Team Type Taxonomy
 */

function tax_video_type() {

  $label_plural   = 'Video Types';
  $label_singular = 'Video Type';

  $labels = array(
    'name'                       => _x( "{$label_plural}", 'Taxonomy General Name', 'boxpress' ),
    'singular_name'              => _x( "{$label_singular}", 'Taxonomy Singular Name', 'boxpress' ),
    'menu_name'                  => __( "{$label_plural}", 'boxpress' ),
    'all_items'                  => __( "All {$label_plural}", 'boxpress' ),
    'parent_item'                => __( "Parent {$label_singular}", 'boxpress' ),
    'parent_item_colon'          => __( "Parent {$label_singular}:", 'boxpress' ),
    'new_item_name'              => __( "New {$label_singular} Name", 'boxpress' ),
    'add_new_item'               => __( "Add New {$label_singular}", 'boxpress' ),
    'edit_item'                  => __( "Edit {$label_singular}", 'boxpress' ),
    'update_item'                => __( "Update {$label_singular}", 'boxpress' ),
    'view_item'                  => __( "View {$label_singular}", 'boxpress' ),
    'separate_items_with_commas' => __( "Separate {$label_plural} with commas", 'boxpress' ),
    'add_or_remove_items'        => __( "Add or remove {$label_plural}", 'boxpress' ),
    'choose_from_most_used'      => __( "Choose from the most used", 'boxpress' ),
    'popular_items'              => __( "Popular {$label_plural}", 'boxpress' ),
    'search_items'               => __( "Search {$label_plural}", 'boxpress' ),
    'not_found'                  => __( "Not Found", 'boxpress' ),
    'items_list'                 => __( "{$label_plural} list", 'boxpress' ),
    'items_list_navigation'      => __( "{$label_plural} list navigation", 'boxpress' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => false,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => false,
    'show_tagcloud'              => false,
    'rewrite'                    => false,
  );

  // For location sorting
  register_taxonomy( 'video_type', 'video', $args );
}
add_action( 'init', 'tax_video_type' );


// Allow sorting by taxonomies in admin
function filter_video_by_taxonomies( $post_type, $which ) {

	// Apply this only on a specific post type
	if ( 'video' !== $post_type )
		return;

	// A list of taxonomy slugs to filter by
	$taxonomies = array( 'video_type' );

	foreach ( $taxonomies as $taxonomy_slug ) {

		// Retrieve taxonomy data
		$taxonomy_obj = get_taxonomy( $taxonomy_slug );
		$taxonomy_name = $taxonomy_obj->labels->name;

		// Retrieve taxonomy terms
		$terms = get_terms( $taxonomy_slug );

		// Display filter HTML
		echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
		echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
		foreach ( $terms as $term ) {
			printf(
				'<option value="%1$s" %2$s>%3$s (%4$s)</option>',
				$term->slug,
				( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
				$term->name,
				$term->count
			);
		}
		echo '</select>';
	}

}
add_action( 'restrict_manage_posts', 'filter_video_by_taxonomies' , 10, 2);
