<?php
/**
 * Image Gallery
 * ---
 * Convert the default Wordpress Gallery into a slideshow.
 *
 * @package boxpress
 */

add_filter( 'post_gallery', 'customFormatGallery', 10, 2 );
function customFormatGallery( $string, $attr ) {
  $posts_order_ids  = $attr['ids'];
  $posts_orderby    = $attr['orderby'];
  $posts_imagesize  = 'full';
  $posts_order      = explode( ',', $posts_order_ids );
  $output           = '';

  if ( array_key_exists( 'size', $attr )) {
    $posts_imagesize  = $attr['size'];
  }

  $posts = get_posts(array(
    'include'   => $posts_order,
    'post_type' => 'attachment',
    'orderby'   => 'post__in',
  ));

  // Randomize order
  if ( $posts_orderby == 'rand' ) {
    shuffle( $posts );
  }
 
  // Set default image size
  if ( empty( $posts_imagesize )) {
    $posts_imagesize = 'full';
  }

  if ( $posts ) {
    // Start Container
    $output .= '<div class="js-carousel image-slideshow">';

    foreach ( $posts as $imagePost ) {
      $image_source  = wp_get_attachment_image_src( $imagePost->ID, $posts_imagesize, false );

      if ( $image_source ) {
        $image_url = $image_source[0];
        $image_width = $image_source[1];
        $image_height = $image_source[2];
        $output .= '<div class="slide-image">';
        $output .= "<img src=\"{$image_url}\" width=\"{$image_width}\" height=\"{$image_height}\" alt=\"\">";
        $output .= '</div>';
      }
    }

    // End Container
    $output .= "</div>";
  }

  return $output;
}
