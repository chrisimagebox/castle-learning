<?php
/**
 * Template header
 *
 * This is the template that displays all of the <head> section and everything up until <main>
 *
 * @package boxpress
 */
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <?php wp_head(); ?>

  <?php // Header Tracking Codes ?>
  <?php the_field( 'header_tracking_codes', 'option' ); ?>

  <script>
    // No js Fallback - Remove 'no-js', add 'js' to html el
    !function(){var n=document.documentElement.classList;n.contains('no-js')&&(n.remove('no-js'),n.add('js'));}();
  </script>
  <link rel="stylesheet" href="https://use.typekit.net/gso7fcj.css">
</head>
<body <?php body_class(); ?>>

<?php // Skip Link ?>
<a class="site-skip-link" href="#main"><?php _e( 'Skip to main content', 'boxpress' ); ?></a>

<?php // SVGs ?>
<?php include( get_template_directory() . '/template-parts/global/svg.php' ); ?>

<?php // Mobile Header ?>
<?php include( get_template_directory() . '/template-parts/global/header-mobile.php' ); ?>

<div class="site-wrap">
<header id="masthead" class="site-header" role="banner">
  <div class="top-bar">
    <div class="wrap">
      <div class="contact-box">
        <?php get_template_part( 'template-parts/global/address-block-top-bar' ); ?>
      </div>
      <div class="contact-social">
        <?php get_template_part( 'template-parts/global/social-nav' ); ?>
      </div>
    </div>
  </div>
  <div class="wrap">
    <div class="l-header">
      <div class="l-header-col-1">

        <div class="site-branding">
          <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
            <span class="vh"><?php bloginfo('name'); ?></span>
            <svg class="site-logo" width="200" height="38" focusable="false">
              <use href="#site-logo"/>
            </svg>
          </a>
        </div>

      </div>
      <div class="l-header-col-2">

        <div class="site-navigation">

          <?php // Main Navigation ?>
          <?php if ( has_nav_menu( 'primary' )) : ?>
            <nav class="js-accessible-menu navigation--main"
              aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
              role="navigation">
              <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
              </ul>
            </nav>
          <?php endif; ?>

        </div>

      </div>
    </div>

  </div>
</header>
<main id="main" class="site-main" role="main">
