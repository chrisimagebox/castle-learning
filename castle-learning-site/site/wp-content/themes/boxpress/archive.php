<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package boxpress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--blog.php'); ?>

  <section class="section blog-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main-col">

          <?php if ( have_posts() ) : ?>

            <header class="page-header">
              <h1 class="page-title"><?php echo get_the_archive_title(); ?></h1>
            </header>

            <div class="l-grid l-grid--three-col">
            <?php while ( have_posts() ) : the_post(); ?>

              <div class="l-grid-item">
                <?php get_template_part( 'template-parts/content/content-preview' ); ?>
              </div>

            <?php endwhile; ?>

            <?php boxpress_pagination(); ?>
          <?php else : ?>

            <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

          <?php endif; ?>

        </div>
      </div>
      <div class="l-aside-col">

        <div class="sidebar">
          <div class="sidebar-widget">
            <nav class="sidebar-nav">
              <ul>
                <?php
                wp_list_pages(array(
                  'child_of' => '93',
                  'depth' => 1,
                  'title_li' => "",
                ));
                ?>
              </ul>
            </nav>
          </div>

          <div class="sidebar-widget">
            <?php // Blog Archive + Post ?>
            <?php if ( is_home() ||
                       is_singular('post') ||
                       ( is_archive() && get_post_type() == 'post' )) :

                /**
                 * Blog Category Links
                 */

                $blog_cats = wp_list_categories( array(
                  'title_li'  => '',
                  'echo'      => false,
                ));
              ?>

              <?php if ( $blog_cats ) : ?>

                <div class="sidebar-widget">
                  <h4 class="widget-title"><?php _e('Categories', 'boxpress'); ?></h4>
                  <nav class="categories-widget">
                    <ul>
                      <li class="<?php
                          if ( is_home() ) {
                            echo 'current-cat';
                          }
                        ?>">
                        <a href="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                          <?php _e( 'All Posts', 'boxpress' ); ?>
                        </a>
                      </li>

                      <?php echo $blog_cats; ?>

                    </ul>
                  </nav>
                </div>

              <?php endif; ?>

              <?php
                /**
                 * Blog Archive Links
                 */

                $blog_archives = wp_get_archives( array(
                  'type'            => 'monthly',
                  'format'          => 'option',
                  'show_post_count' => 1,
                  'echo'            => false,
                ));
              ?>

              <?php if ( $blog_archives ) : ?>

                <div class="sidebar-widget">
                  <h4 class="widget-title"><?php _e('Archives', 'boxpress'); ?></h4>
                  <div class="archives-widget">
                    <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                      <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
                      <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
                        <option value=""><?php echo _e( 'Select Month' ); ?></option>
                        <?php echo $blog_archives; ?>
                      </select>
                    </form>
                  </div>
                </div>

              <?php endif; ?>

              <?php
                /**
                 * Blog Tag Links
                 */

                $tags = get_tags( array(
                  'orderby' => 'count',
                  'order'   => 'DESC',
                ));
              ?>

              <?php if ( $tags ) : ?>

                <div class="sidebar-widget">
                  <h4 class="widget-title"><?php _e('Tags', 'boxpress'); ?></h4>
                  <div class="tags-widget">
                    <ul>

                      <?php foreach ( $tags as $tag ) : ?>

                        <li>
                          <a href="<?php echo get_tag_link( $tag->term_id ); ?>">
                            <?php echo $tag->name; ?>
                          </a>
                        </li>

                      <?php endforeach; ?>

                    </ul>
                  </div>
                </div>

              <?php endif; ?>
            <?php endif; ?>
          </div>
        </div>

      </div>
    </div>
  </section>

<?php get_footer(); ?>
