<?php
/**
 * Displays the preview content block
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--preview' ); ?>>

  <div class="card">
    <?php if (has_post_thumbnail( $post ) ): ?>
    <div class="card-header">
        <a href="<?php the_permalink(); ?>">
          <?php the_post_thumbnail('card_thumb');?>
        </a>
    </div>
  <?php endif; ?>
    <div class="card-footer">
      <div class="entry-meta">
        <?php boxpress_posted_on(); ?>
      </div>
      <h4><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h4>
      <?php the_excerpt(); ?>
    </div>
  </div>


</article>
