<?php
/**
 * The template part for displaying results in search pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>



    <div class="l-grid-item">
      <?php
      $ebook_photo = get_sub_field('ebook_photo');
      $ebook_photo_size = 'vid_thumb';
      $ebook_heading = get_sub_field('ebook_heading');
      $ebook_text = get_sub_field('ebook_text');
      $ebook_form = get_sub_field('ebook_form');
      ?>
        <div class="card">
          <div class="card-header">
                <img
                  src="<?php echo esc_url( $ebook_photo['sizes'][ $ebook_photo_size ] ); ?>"
                  width="<?php echo esc_attr( $ebook_photo['sizes'][ $ebook_photo_size . '-width'] ); ?>"
                  height="<?php echo esc_attr( $ebook_photo['sizes'][ $ebook_photo_size . '-height'] ); ?>"
                  alt="<?php echo esc_attr( $ebook_photo['alt'] ); ?>">
          </div>
          <div class="card-footer">
            <h1 class="entry-title">
              <?php echo $ebook_heading; ?>
            </h1>
        <p><?php echo $ebook_text; ?></p>
            <a class="button button-2 open-form" href="#popup-<?php echo get_the_ID(); ?>">
            Download Link
            </a>

            <div id="popup-<?php echo get_the_ID(); ?>" class="mfp-hide popup">
              <div class="wrap modal-wrap">
                <div class="modal-content">
                  <div class="modal-header">
                    <button title="Close (Esc)" type="button" class="mfp-close">  <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
                      <svg class="menu-icon-svg" width="20" height="20" focusable="false">
                        <use href="#close-icon"/>
                      </svg><span class="custom-close"></span></button>
                       <?php echo $ebook_form; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
