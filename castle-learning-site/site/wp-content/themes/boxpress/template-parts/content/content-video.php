<?php
/**
 * The template part for displaying results in search pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>



    <div class="l-grid-item">
      <?php
      $vid_thumbnail = get_field('vid_thumbnail');
      $vid_thumbnail_size = 'vid_thumb';
      $vid_thumbnail_link = get_field('vid_thumbnail_link');
      ?>
      <?php if ( $vid_thumbnail_link ) : ?>
        <div class="card">
          <a class="video-button" href="<?php echo esc_url($vid_thumbnail_link); ?>">
          <div class="card-header">
                <img
                  src="<?php echo esc_url( $vid_thumbnail['sizes'][ $vid_thumbnail_size ] ); ?>"
                  width="<?php echo esc_attr( $vid_thumbnail['sizes'][ $vid_thumbnail_size . '-width'] ); ?>"
                  height="<?php echo esc_attr( $vid_thumbnail['sizes'][ $vid_thumbnail_size . '-height'] ); ?>"
                  alt="<?php echo esc_attr( $vid_thumbnail['alt'] ); ?>">
          <div class="svg-vid"></div>
          </div>
          <div class="card-footer">
            <h1 class="entry-title">
            <?php the_title(); ?>
            </h1>
            <?php
              the_content( sprintf(
                __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'boxpress' ),
                the_title( '<span class="screen-reader-text">"', '"</span>', false )
              ));
            ?>
          </div>
         </a>
        </div>
       <?php endif; ?>
    </div>
