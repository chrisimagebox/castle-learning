<?php
/**
 * The template part for displaying results in search pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>



    <div class="l-grid-item">
      <?php
      $assignment_photo = get_sub_field('assignment_photo');
      $assignment_photo_size = 'vid_thumb';
      $assignment_heading = get_sub_field('assignment_heading');
      $assignment_text = get_sub_field('assignment_text');
      $assignment_form = get_sub_field('assignment_form');
      ?>
        <div class="card">
          <div class="card-header">
                <img
                  src="<?php echo esc_url( $assignment_photo['sizes'][ $assignment_photo_size ] ); ?>"
                  width="<?php echo esc_attr( $assignment_photo['sizes'][ $assignment_photo_size . '-width'] ); ?>"
                  height="<?php echo esc_attr( $assignment_photo['sizes'][ $assignment_photo_size . '-height'] ); ?>"
                  alt="<?php echo esc_attr( $assignment_photo['alt'] ); ?>">
          </div>
          <div class="card-footer">
            <h1 class="entry-title">
              <?php echo $assignment_heading; ?>
            </h1>
        <p><?php echo $assignment_text; ?></p>
            <a class="button button-2 open-form" href="#popup-<?php echo get_the_ID(); ?>">
              Download Link
            </a>

            <div id="popup-<?php echo get_the_ID(); ?>" class="mfp-hide popup">
              <div class="wrap modal-wrap">
                <div class="modal-content">
                  <div class="modal-header">
                    <button title="Close (Esc)" type="button" class="mfp-close">  <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
                      <svg class="menu-icon-svg" width="20" height="20" focusable="false">
                        <use href="#close-icon"/>
                      </svg><span class="custom-close"></span></button>
                       <?php echo $assignment_form; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
