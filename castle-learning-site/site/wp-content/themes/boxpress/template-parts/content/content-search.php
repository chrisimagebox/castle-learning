<?php
/**
 * The template part for displaying results in search pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package boxpress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'content--search' ); ?>>
  <header class="entry-header">
    <h2 class="entry-title">
      <a href="<?php the_permalink(); ?>">
        <?php the_title(); ?>
      </a>
    </h2>

    <?php if ( 'post' == get_post_type() ) : ?>
      <div class="entry-meta">
        <?php boxpress_posted_on(); ?>
      </div>
    <?php endif; ?>
  </header>

  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>

  <footer class="entry-footer">
    <a class="text-button" href="<?php the_permalink(); ?>"><?php _e('Read More', 'boxpress'); ?></a>
  </footer>
</article>
