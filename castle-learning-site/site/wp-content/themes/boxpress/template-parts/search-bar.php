<?php
/**
 * Displays the Search Bar
 *
 * @package boxpress
 */
?>
<div class="search-bar">
  <form class="search-form" method="get" action="<?php echo home_url( '/' ); ?>">
    <label>
      <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
      <input class="search-field" type="search" name="s"
        placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>"
        value="<?php echo get_search_query() ?>">
    </label>
    <button type="submit" class="button search-submit">
      <span class="vh"><?php _e( 'Search', 'boxpress' ); ?></span>
      <svg class="search-svg" width="15" height="15" focusable="false">
        <use href="#search-icon"/>
      </svg>
    </button>
  </form>
</div>
