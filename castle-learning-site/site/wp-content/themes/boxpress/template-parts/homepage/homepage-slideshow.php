<?php
/**
 * Displays the Slideshow layout
 *
 * @package boxpress
 */
?>
<?php if ( have_rows( 'home_carousel' )) : ?>

  <div class="home-carousel">
    <div class="js-carousel">

      <?php while ( have_rows( 'home_carousel' )) : the_row(); ?>
        <?php
          $slide_content  = get_sub_field( 'slide_content' );
          $slide_bkg      = get_sub_field( 'slide_bkg' );
          $make_it_right      = get_sub_field( 'make_it_right' );
        ?>

        <div class="carousel-slide">

          <?php if ( $slide_bkg ) : ?>
            <?php
              $image_size = 'home_slideshow';
            ?>
            <img class="slide-bkg" draggable="false" aria-hidden="true"
              src="<?php echo $slide_bkg['sizes'][ $image_size ]; ?>"
              width="<?php echo $slide_bkg['sizes'][ $image_size . '-width' ]; ?>"
              height="<?php echo $slide_bkg['sizes'][ $image_size . '-height' ]; ?>"
              alt="">
          <?php endif; ?>

          <div class="wrap slide-wrap <?php echo $make_it_right; ?>">
            <div class="slide-content">
              <?php echo $slide_content; ?>
            </div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>
  </div>
<?php endif; ?>

<div class="hero-callout-box">
  <div class="wrap">
   <div class="box-content">
      <div class="box-left">
        <?php
         $hero_callout_box_text_left = get_field('hero_callout_box_text_left');
         ?>
        <h2><?php echo $hero_callout_box_text_left; ?></h2>
      </div>


        <?php if ( have_rows( 'header_links' )) : ?>
          <div class="box-right">
              <?php while ( have_rows( 'header_links' )) : the_row(); ?>
                <?php
                    $box_photo = get_sub_field('box_photo');
                    $box_link = get_sub_field('box_link');
                 ?>

                  <a class="box" href="<?php echo esc_url( $box_link['url'] ); ?>"
                        target="<?php echo esc_attr( $box_link['target'] ); ?>">
                    <img src="<?php echo $box_photo; ?>" alt="<?php echo $box_photo; ?>">
                    <span><?php echo $box_link['title']; ?></span>
                  </a>

              <?php endwhile; ?>
            <?php endif; ?>
          </div>
    </div>
  </div>
</div>
