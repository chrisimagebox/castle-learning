<?php
/**
 * Mobile Header
 *
 * @package boxpress
 */
?>
<header class="site-header--mobile" role="banner">
  <div class="mobile-header-left">
    <div class="site-branding">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <span class="vh"><?php bloginfo('name'); ?></span>
        <svg class="site-logo" width="253" height="48" focusable="false">
          <use href="#site-logo"/>
        </svg>
      </a>
    </div>
  </div>
  <div class="mobile-header-right">
    <button type="button" class="js-toggle-nav menu-button"
      aria-controls="mobile-nav-tray"
      aria-expanded="false">
      <span class="vh"><?php _e('Menu', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="48" height="33" focusable="false">
        <use href="#menu-icon"/>
      </svg>
    </button>
  </div>
</header>

<div id="mobile-nav-tray" class="mobile-nav-tray" aria-hidden="true">
  <div class="site-branding">
    <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
      <span class="vh"><?php bloginfo('name'); ?></span>
      <svg class="site-logo" width="253" height="48" focusable="false">
        <use href="#site-logo"/>
      </svg>
    </a>
  </div>
  <div class="mobile-nav-header">
    <button type="button" class="js-toggle-nav close-button"
      aria-controls="mobile-nav-tray"
      aria-expanded="false">
      <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="33" height="33" focusable="false">
        <use href="#close-icon"/>
      </svg>
    </button>
  </div>
  <nav class="navigation--mobile">

    <?php if ( has_nav_menu( 'primary' )) : ?>
      <ul class="mobile-nav mobile-nav--main">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'primary',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    <?php endif; ?>

    <?php if ( has_nav_menu( 'secondary' )) : ?>
      <ul class="mobile-nav mobile-nav--utility">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'secondary',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    <?php endif; ?>

  </nav>
  <div class="top-bar">
    <div class="wrap">
      <div class="contact-box">
        <?php get_template_part( 'template-parts/global/address-block-top-bar' ); ?>
      </div>
      <div class="contact-social">
        <?php get_template_part( 'template-parts/global/social-nav' ); ?>
      </div>
    </div>
  </div>
</div>
