<?php
/**
 * Site address and contact info
 *
 * Note - No heading tags allowed in `address` tag :(
 *
 * @package boxpress
 */

$organization_type = get_field( 'organization_type', 'option' );

if ( empty( $organization_type )) {
  $organization_type = 'Organization';
}

?>
<?php if ( have_rows( 'site_offices', 'option' )) : ?>

  <address class="address-block" itemscope itemtype="https://schema.org/<?php echo $organization_type; ?>">
    <?php
      $company_name     = get_bloginfo( 'name', 'display' );
      $alt_company_name = get_field( 'alternative_company_name', 'option' );

      if ( ! empty( $alt_company_name )) {
        $company_name = $alt_company_name;
      }
    ?>
    <p class="address-title h5">
      <span itemprop="name"><?php echo $company_name; ?></span>
    </p>

    <?php while ( have_rows( 'site_offices', 'option' )) : the_row(); ?>
      <?php
        $street_address = get_sub_field('street_address');
        $address_line_2 = get_sub_field('address_line_2');
        $city           = get_sub_field('city');
        $state          = get_sub_field('state');
        $zip            = get_sub_field('zip');
        $country        = get_sub_field('country');
        $phone_number   = get_sub_field('phone_number');
        $fax_number     = get_sub_field('fax_number');
        $email          = get_sub_field('email');

        $full_street_address = $street_address;

        if ( ! empty( $address_line_2 )) {
          $full_street_address = "{$street_address}, {$address_line_2}";
        }
      ?>
      <div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
        <p>
          <?php if ( ! empty( $street_address )) : ?>
            <span itemprop="streetAddress"><?php echo $full_street_address; ?></span><br>
          <?php endif; ?>

          <?php if ( ! empty( $city )) : ?>
            <span itemprop="addressLocality"><?php echo $city; ?></span>, 
          <?php endif; ?>

          <?php if ( ! empty( $state )) : ?>
            <span itemprop="addressRegion"><?php echo $state; ?></span> 
          <?php endif; ?>

          <?php if ( ! empty( $zip )) : ?>
            <span itemprop="postalCode"><?php echo $zip; ?></span> 
          <?php endif; ?>

          <?php if ( ! empty( $country )) : ?>
            <span class="vh" itemprop="addressCountry"><?php echo $country; ?></span>
          <?php endif; ?>
        </p>
      </div>

      <?php if ( ! empty( $phone_number ) || ! empty( $email )) : ?>

        <div class="address-contact-info">
          <?php if ( ! empty( $phone_number )) : ?>
            <?php
              // Strip hyphens & parenthesis for tel link
              $tel_formatted = str_replace([ ".", "-", "–", "(", ")", " " ], '', $phone_number );
            ?>
            <p>
              <span class="vh"><?php _e( 'Phone:', 'boxpress' ); ?></span>
              <a href="tel:+1<?php echo $tel_formatted; ?>">
                <span itemprop="telephone"><?php echo $phone_number; ?></span>
              </a>
            </p>
          <?php endif; ?>

          <?php if ( ! empty( $fax_number )) : ?>
            <p>
              <span class="vh"><?php _e( 'Fax:', 'boxpress' ); ?></span>
              <span itemprop="faxNumber"><?php echo $fax_number; ?></span>
            </p>
          <?php endif; ?>

          <?php if ( ! empty( $email )) : ?>
            <p>
              <span class="vh"><?php _e( 'Email:', 'boxpress' ); ?></span>
              <a class="email" href="mailto:<?php echo $email; ?>">
                <span itemprop="email"><?php echo $email; ?></span>
              </a>
            </p>
          <?php endif; ?>
        </div>

      <?php endif; ?>
    <?php endwhile; ?>

  </address>

<?php endif; ?>
