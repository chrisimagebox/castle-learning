<?php
/**
 * The template for displaying search results pages.
 *
 * @package boxpress
 */

get_header(); ?>

  <?php require_once('template-parts/banners/banner--search.php'); ?>

  <section class="fullwidth-column section">
    <div class="wrap wrap--limited">

      <?php if ( have_posts() ) : ?>

        <header class="page-header">
          <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'boxpress' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        </header>

        <?php while ( have_posts() ) : the_post(); ?>

          <?php get_template_part( 'template-parts/content/content', 'search' ); ?>

        <?php endwhile; ?>

        <?php boxpress_pagination(); ?>
      <?php else : ?>
        <?php get_template_part( 'template-parts/content/content', 'none' ); ?>
      <?php endif; ?>

    </div>
  </section>
<?php get_footer(); ?>
