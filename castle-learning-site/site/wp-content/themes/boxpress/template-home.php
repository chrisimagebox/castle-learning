<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php // Hero ?>
    <?php get_template_part( 'template-parts/homepage/homepage-slideshow' ); ?>


    <?php
    $home_split_content    = get_field( 'home_split_content' );
    $home_split_image      = get_field( 'home_split_image' );
    $home_split_image_size = 'block_half_width';
    ?>


  <section class="section home-split-block">
    <div class="photo-block">
      <img class="split-block-image"
        src="<?php echo esc_url( $home_split_image['sizes'][ $home_split_image_size ] ); ?>"
        width="<?php echo esc_attr( $home_split_image['sizes'][ $home_split_image_size . '-width'] ); ?>"
        height="<?php echo esc_attr( $home_split_image['sizes'][ $home_split_image_size . '-height'] ); ?>"
        alt="<?php echo esc_attr( $home_split_image['alt'] ); ?>">
    </div>

    <div class="content-block">
      <div class="split-wrap">
         <?php echo $home_split_content; ?>
      </div>
    </div>
  </section>

  <section class="section">
    <div class="wrap wrap--limited">
    <div class="home-header">
      <?php $home_heading = get_field('home_heading') ?>
      <?php echo $home_heading; ?>
    </div>

      <?php if ( have_rows( 'home_columns' )) : ?>
        <div class="home-columns">
            <?php while ( have_rows( 'home_columns' )) : the_row();  ?>

              <?php  $home_column_content = get_sub_field('home_column_content'); ?>

          <div class="home-col">
            <?php echo $home_column_content; ?>
          </div>

        <?php endwhile; ?>
        </div>
    <?php endif; ?>
    </div>
  </section>

<section class="section home-callout">
  <?php

    $home_callout_header = get_field('home_callout_header');
    $home_callout_header_link_1 = get_field('home_callout_header_link_1');
    $home_callout_header_link_2 = get_field('home_callout_header_link_2');

   ?>

  <div class="callout-content">
    <div class="wrap">
      <div class="callout-header">
        <h3><?php echo $home_callout_header;  ?></h3>
      </div>
      <div class="callout-buttons">
          <a href="#form" class="button open-form">
            Contact Us Now
          </a>
       <div id="form" class="mfp-hide popup">
         <div class="">
           <!-- Modal content -->
           <div class="modal-content">
              <?php echo gravity_form(3, false, false, false, '', true, 12);  ?>
           </div>
         </div>
       </div>
        <?php if ( $home_callout_header_link_2 ) : ?>
          <a class="button"
            href="<?php echo esc_url( $home_callout_header_link_2['url'] ); ?>"
            target="<?php echo esc_attr( $home_callout_header_link_2['target'] ); ?>">
            <?php echo $home_callout_header_link_2['title']; ?>
          </a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>

<?php
$home_split_header_bottom    = get_field( 'home_split_header_bottom' );
$home_split_link_bottom_1      = get_field( 'home_split_link_bottom_1' );
$home_split_link_bottom_2      = get_field( 'home_split_link_bottom_2' );
$home_split_icon_bottom_1      = get_field( 'home_split_icon_bottom_1' );
$home_split_icon_bottom_2      = get_field( 'home_split_icon_bottom_2' );
?>


<section class="section home-split-block--bottom">
<div class="wrap">
  <div class="content-block">
      <div class="home-split-header">
        <?php echo $home_split_header_bottom; ?>
      </div>
    </div>

  <div class="photo-block">
    <?php if ($home_split_link_bottom_1) : ?>
      <a class="link-box" href="<?php echo esc_url( $home_split_link_bottom_1['url'] ); ?>"
            target="<?php echo esc_attr( $home_split_link_bottom_1['target'] ); ?>">
        <img src="<?php echo $home_split_icon_bottom_1; ?>" alt="<?php echo $home_split_icon_bottom_1; ?>">
        <span class="link-text">Support</span>
        <span class="link-button"><?php echo $home_split_link_bottom_1['title']; ?></span>
      </a>
    <?php endif; ?>

    <?php if ($home_split_link_bottom_2) : ?>
      <a class="link-box" href="<?php echo esc_url( $home_split_link_bottom_2['url'] ); ?>"
            target="<?php echo esc_attr( $home_split_link_bottom_2['target'] ); ?>">
        <img src="<?php echo $home_split_icon_bottom_2; ?>" alt="<?php echo $home_split_icon_bottom_2; ?>">
        <span class="link-text">Professional Development</span>
        <span class="link-button"><?php echo $home_split_link_bottom_2['title']; ?></span>
      </a>
    <?php endif; ?>
  </div>
</div>
</section>



    <?php // Latest Posts Example ?>
    <?php
      $home_post_query_args = array(
        'post_type' => 'post',
        'posts_per_page' => 3
      );
      $home_post_query = new WP_Query( $home_post_query_args );
    ?>
    <?php if ( $home_post_query->have_posts() ) : ?>

    <?php
      $home_blog_heading    = get_field( 'home_blog_heading' );
      $home_blog_link    = get_field( 'home_blog_link' );
     ?>

      <section class="section home-blog-section">
        <div class="wrap wrap--limited">
          <div class="home-blog-header">
            <h2><?php echo $home_blog_heading; ?></h2>
          </div>
          <div class="l-grid l-grid--three-col">

            <?php while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>

              <div class="l-grid-item">
                  <?php if (has_post_thumbnail( $post ) ): ?>
                  <div class="featured-blog-photo">
                    <a href="<?php the_permalink(); ?>">
                      <?php the_post_thumbnail('card_thumb');?>
                    </a>
                  </div>
                   <?php endif; ?>
                  <h3><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
                  <?php the_excerpt(); ?>
              </div>

            <?php endwhile; ?>
          </div>
          <div class="home-blog-footer">
            <?php if ( $home_blog_link ) : ?>
              <a class="button"
                href="<?php echo esc_url( $home_blog_link['url'] ); ?>"
                target="<?php echo esc_attr( $home_blog_link['target'] ); ?>">
                <?php echo $home_blog_link['title']; ?>
              </a>
            <?php endif; ?>
          </div>
        </div>
      </section>

      <?php wp_reset_postdata(); ?>
    <?php endif; ?>

    <?php
    $home_form_photo  = get_field( 'home_form_photo' );
    $home_form_photo_size = 'block_full_width';
     ?>

    <section class="section home-form-section">
      <img class="form-photo"
        src="<?php echo esc_url( $home_form_photo['sizes'][ $home_form_photo_size ] ); ?>"
        width="<?php echo esc_attr( $home_form_photo['sizes'][ $home_form_photo_size . '-width'] ); ?>"
        height="<?php echo esc_attr( $home_form_photo['sizes'][ $home_form_photo_size . '-height'] ); ?>"
        alt="<?php echo esc_attr( $home_form_photo['alt'] ); ?>">

      <div class="home-form-content">
        <div class="wrap wrap--limited">
           <?php gravity_form( 1, true, true, false, '', false ); ?>
        </div>
      </div>
    </section>


  </article>

<?php get_footer(); ?>
